FROM node:lts-alpine

WORKDIR /app

COPY . .

ENV HTTP_PORT=5000
RUN npm install --production

EXPOSE 5000/tcp
ENTRYPOINT ["node", "./index.js"]

CMD []
