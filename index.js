const express = require('express')
const morgan = require('morgan');
const http = require('http')
const app = express()
const os = require('os');
const concat = require('concat-stream');

app.set('json spaces', 2);
app.use(morgan('combined'));

app.use(function (req, res, next) {
    req.pipe(concat(function (data) {
        req.body = data.toString('utf8');
        next();
    }));
});

app.all('*', (req, res) => {
    const echo = {
        path: req.path,
        headers: req.headers,
        method: req.method,
        body: req.body,
        cookies: req.cookies,
        fresh: req.fresh,
        hostname: req.hostname,
        ip: req.ip,
        ips: req.ips,
        protocol: req.protocol,
        query: req.query,
        subdomains: req.subdomains,
        xhr: req.xhr,
        os: {
            hostname: os.hostname()
        },
        connection: {
            servername: req.connection.servername
        }
    };
    res.json(echo);
});

const httpServer = http.createServer(app).listen(process.env.HTTP_PORT || 5000);

let calledClose = false;

process.on('exit', function () {
    if (calledClose) return;
    console.log('Got exit event. Trying to stop Express server.');
    server.close(function () {
        console.log("Express server closed");
    });
});

process.on('SIGINT', shutDown);
process.on('SIGTERM', shutDown);

function shutDown() {
    console.log('Got a kill signal. Trying to exit gracefully.');
    calledClose = true;
    httpServer.close(function () {
        process.exit()
    });
}
